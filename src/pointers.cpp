#include <iostream>

using namespace std;

void myswap(int * ptr1, int* ptr2)
{
  auto temp = *ptr1;
  *ptr1 = *ptr2;
  *ptr2 = temp;// theses pointers must be passed with * before otherwise the program
  // wont run 
}


int main()
{
  int a = 25, b = 11;    

  cout<<"What am I doing wrong ☹ \n\n";

  cout<<"a = "<<a<<", b = "<<b<<endl;

//swap(a,b); //why does theirs work????   
 // the function call must be correct
 myswap (&a , &b);
  // trying the swap function 
  cout<<"a = "<<a<<", b = "<<b<<endl;


}
